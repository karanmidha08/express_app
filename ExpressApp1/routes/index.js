'use strict';
var express = require('express');
var router = express.Router();

var getData = function () {
    var data = {
        'item1': 'https://th.bing.com/th/id/R.8dccd06cd431b279dc46bb16b02211d4?rik=EahJ%2fzDvQ8WhmA&riu=http%3a%2f%2fthewowstyle.com%2fwp-content%2fuploads%2f2015%2f04%2ftuned_concept_pink_car-normal.jpg&ehk=mEutOJv331Z3kGALyozxCFRZLTKtHMkGGFR%2b3aC6erU%3d&risl=&pid=ImgRaw',
        'item2': 'https://th.bing.com/th/id/R.4d0fb3e6380dce043c5248b65f03bc1f?rik=E666ZovIynOMaA&riu=http%3a%2f%2fwww.hdcarwallpapers.com%2fdownload%2flamborghini_gallardo_orange-2560x1600.jpg&ehk=zkW%2ffLa8p8ZKh%2b24jwnLw7W%2fBpiK9bIBBsmDtC1VTok%3d&risl=&pid=ImgRaw',
        'item3': 'http://public-domain-photos.com/free-stock-photos-1/flowers/cactus-78.jpg'
    }
    return data;
}

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', { title: 'Express', "data": getData() });
});

module.exports = router;
